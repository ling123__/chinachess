/****************************************************
    文件：LoginSys.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/7/11 19:26:26
	功能：业务系统初始化
*****************************************************/

using UnityEngine;

public class LoginSys : MonoBehaviour
{
    private static LoginSys instance = null;
    public static LoginSys Instance { get => instance; }

    public void Init()
    {
        instance = this;
    }
    //服务器返回登陆情况
    public void RspLogin(NetMsg msg)
    {
        //提示  游戏登陆成功
        Debug.Log("Info:登陆成功");
        //设置用户数据
        GameManager.Instance.SetPlayerData(msg.rspLogin.playerData);
        Debug.Log(msg.color);
        GameManager.Instance.currentChess = "红色";
        GameManager.Instance.SetChessColor(msg.color);  //设置本地棋子颜色

        //进入大厅
        UIManager.Instance.PushPanel(UIPanelType.NetPanel);
    }

    public void RspPlayChess(NetMsg msg)
    {
        RspPlayChess rsp = msg.rspPlayChess;
        NetPanel.Instance.UpdateChessGrid(rsp);
        GameManager.Instance.currentChess = rsp.color;
    }
}