/****************************************************
    文件：ClientRoot.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/7/11 19:25:5
	功能：客户端初始化
*****************************************************/

using PENet;
using UnityEngine;

public class ClientRoot 
{
    private static ClientRoot instance = null;
    public static ClientRoot Instance
    {
        get
        {
            if (instance == null)
                instance = new ClientRoot();
            return instance;
        }
    }

    public void Init()
    {
        //服务层
        NetSvc.Instance.Init();
        //业务层
        LoginSys.Instance.Init();
    }
}