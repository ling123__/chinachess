/****************************************************
    文件：NetSvc.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/7/11 19:25:44
	功能：服务端初始化
*****************************************************/

using PENet;
using System.Collections.Generic;
using UnityEngine;

public class NetSvc :MonoBehaviour
{
    private static NetSvc instance = null;
    public static NetSvc Instance { get => instance; }

    PESocket<ClientSession, NetMsg> client = null;
    public void Init()
    {
        instance = this;

        client = new PESocket<ClientSession, NetMsg>();
        client.StartAsClient(IPCfg.srvIP, IPCfg.srvPort);

        //设置日志接口，根据信息等级输出
        client.SetLog(true, (string text, int lv) =>
        {
            switch (lv)
            {
                case 0:
                    Debug.Log("Log:" + text);
                    break;
                case 1:
                    Debug.LogWarning("Warn:" + text);
                    break;
                case 2:
                    text = "Error:" + text;
                    Debug.LogError(text);
                    break;
                case 3:
                    Debug.Log("Info:" + text);
                    break;
            }
        });
        PECommon.Log("Init Server...", LogType.Info);
    }
    private static readonly string obj = "lock";
    private Queue<NetMsg> MsgQueue = new Queue<NetMsg>();

    public void SendMsg(NetMsg msg)
    {
        if (client.session != null)
            client.session.SendMsg(msg);
        else
        {
            //提示  服务器未连接
            Init();
        }
    }

    public void AddMessage(NetMsg msg)
    {
        lock (MsgQueue)
        {
            MsgQueue.Enqueue(msg);
        }
    }

    public void Update()
    {
        if(MsgQueue.Count > 0)
        {
            lock (obj)
            {
                NetMsg msg = MsgQueue.Dequeue();
                HandOutMsg(msg);
            }
        }
    }

    public void HandOutMsg(NetMsg msg)
    {
        //如果err不为空说明有错误
        if (msg.err != (int)ErrorCode.None)  
        {
            switch ((ErrorCode)msg.err)
            {
                case ErrorCode.AcctIsOnLine:
                    //提示  游戏已经登陆
                    Debug.Log("账号已经登陆");
                    break;
                case ErrorCode.WrongPass:
                    //提示  账号或者密码错误
                    Debug.Log("账号或者密码错误");
                    break;
            }
            return;
        }
        switch ((CMD)msg.cmd)
        {
            case CMD.RspLogin:
                LoginSys.Instance.RspLogin(msg);
                break;
            case CMD.RspPlayChess:
                LoginSys.Instance.RspPlayChess(msg);
                break;
        }
    }

}