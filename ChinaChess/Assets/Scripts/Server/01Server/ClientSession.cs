/****************************************************
    文件：ClientSession.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/7/11 10:44:59
	功能：客户端会话
*****************************************************/

using UnityEngine;
using PENet;

public class ClientSession : PESession<NetMsg> 
{
    protected override void OnConnected()
    {
        Debug.Log("Server Connect");
    }

    protected override void OnReciveMsg(NetMsg msg)
    {
        Debug.Log("Server Req:" + ((CMD)msg.cmd).ToString());
        NetSvc.Instance.AddMessage(msg);
    }

    protected override void OnDisConnected()
    {
        Debug.Log("Server DisConnect");
    }
}