/****************************************************
    文件：BasePanel.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/2 21:45:34
	功能：UI面板基类
*****************************************************/

using UnityEngine;

public class BasePanel : MonoBehaviour 
{
    //面板显示
    public virtual void OnUIEnter()
    {

    }
    //面板隐藏
    public virtual void OnUIPause()
    {

    }
    //面板再次显示
    public virtual void OnUIResume()
    {

    }
    //移除面板
    public virtual void OnUIExit()
    {

    }
}