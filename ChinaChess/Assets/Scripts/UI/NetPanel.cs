/****************************************************
    文件：GamePanel.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 8:59:11
	功能：游戏界面
*****************************************************/
using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

public class NetPanel : BasePanel
{
    public Sprite[] blackChess;
    public Sprite[] readChess;
    public Transform ChessGridParent;

    private CanvasGroup m_canvasGroup;
    private Button btn_Return;
    private GameObject[,] chessGridObject = new GameObject[10, 9];   //棋盘

    private string[] chessBName = { "黑兵", "黑炮", "黑车", "黑马", "黑象", "黑士", "黑将", };
    private string[] chessRName = { "红兵", "红炮", "红车", "红马", "红象", "红士", "红将", };

    private GameObject currentChess;
    private GameObject firstChess;
    private GameObject blankGrid;
    private int[] temp = new int[4];

    private static NetPanel instance;
    public static NetPanel Instance { get => instance; }

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        btn_Return = transform.Find("Return").GetComponent<Button>();

        btn_Return.onClick.AddListener(ReturnTrick);
        GameManager.Instance.chessGridMap = LoadFile.Instance.MapInfo;
        ChessBoard();    //生成棋盘
        //TODO 开始游戏动画

    }

    private void Update()
    {
        if (!GameManager.Instance.isGameOver)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    //根据tag分别下不同的棋
                    if (hit.transform.tag == Consts.ChessGrid)
                    {
                        PlayGridChess(hit.transform.gameObject);
                    }
                    else if (hit.transform.tag == Consts.ChessBlack)
                    {
                        PlayBlackChess(hit.transform.gameObject);
                    }
                    else if (hit.transform.tag == Consts.ChessRed)
                    {
                        PlayRedChess(hit.transform.gameObject);
                    }
                }
            }
        }
    }
    private void PlayGridChess(GameObject go)
    {
        if (firstChess == null) return;
        blankGrid = go;
        ParseStringToInt(firstChess, blankGrid);
        //GameManager.Instance.chessObject[temp[0], temp[1]] = null;
        
        //如果可以下棋
        if (firstChess.GetComponent<ChessInfo>().PlayChessStyle(temp[0], temp[1], temp[2], temp[3]))
        {
            SendToServer();
        }
    }
    private void PlayBlackChess(GameObject go)
    {
        if (!GameManager.Instance.isGameOver)
        {
            if (GameManager.Instance.currentChess == GameManager.Instance.ChessColor)  //开始
            {
                firstChess = go;
            }
            else
            {
                currentChess = go;
                if (firstChess != null)
                {
                    ParseStringToInt(firstChess, currentChess);
                    if (firstChess.GetComponent<ChessInfo>().PlayChessStyle(temp[0], temp[1], temp[2], temp[3]))
                    {
                        SendToServer();
                    }
                }
            }
        }
    }
    private void PlayRedChess(GameObject go)
    {
        if (!GameManager.Instance.isGameOver)
        {
            if (GameManager.Instance.currentChess == GameManager.Instance.ChessColor)  //开始默认是红方先下
            {
                firstChess = go;
            }
            else
            {
                currentChess = go;
                if (firstChess != null)
                {
                    ParseStringToInt(firstChess, currentChess);
                    if (firstChess.GetComponent<ChessInfo>().PlayChessStyle(temp[0], temp[1], temp[2], temp[3]))
                    {
                        SendToServer();
                    }
                }
            }
        }
    }
    //向服务器发送消息
    private void SendToServer()
    {
        //如果当前位置可以下棋，将信息传回给服务器，再由服务器统一调度
        NetMsg msg = new NetMsg
        {
            cmd = (int)CMD.ReqPlayChess,
            reqPlayChess = new ReqPlayChess
            {
                fromx = temp[0],
                tox = temp[2],
                fromy = temp[1],
                toy = temp[3],
                color = GameManager.Instance.ChessColor,
                firstChess = firstChess.name,
                currentChess = currentChess == null ? "" : currentChess.name,
                blankGrid = blankGrid == null ?"":blankGrid.name
            }
        };
        NetSvc.Instance.SendMsg(msg);  //发送给服务器
    }
    //响应服务器操作，设置棋盘 
    public void UpdateChessGrid(RspPlayChess rsp)
    {
        firstChess = GameObject.Find(rsp.firstChess);
        Debug.Log(rsp.firstChess);
        if (rsp.currentChess != "")
            currentChess = GameObject.Find(rsp.currentChess);
        if (rsp.blankGrid != "")
            blankGrid = GameObject.Find(rsp.blankGrid);

        if(currentChess != null)
            Destroy(currentChess);
        GameManager.Instance.chessObject[rsp.fromx, rsp.fromy] = null;
        GameManager.Instance.chessObject[rsp.tox, rsp.toy] = firstChess;
        //如果吃到了敌方的主将，或者两将碰面游戏结束
        if (firstChess.GetComponent<ChessInfo>().JudgeChess(rsp.tox, rsp.toy)
            || firstChess.GetComponent<ChessInfo>().EatBoss(rsp.tox, rsp.toy))
        {
            ChessTips.Instance.WinTip();
        }
        //TODO 被吃了
        if (currentChess != null)
            MoveInit(currentChess.transform.parent.gameObject);
        else
            MoveInit(blankGrid);
    }
    //提取物体名字中的位置
    private void ParseStringToInt(GameObject t1, GameObject t2)
    {
        temp[0] = Int32.Parse(t1.name.Substring(2, 1));
        temp[1] = Int32.Parse(t1.name.Substring(4, 1));
        temp[2] = Int32.Parse(t2.name.Substring(2, 1));
        temp[3] = Int32.Parse(t2.name.Substring(4, 1));
    }
    //棋子移动后的初始化
    private void MoveInit(GameObject go)
    {
        //更新棋子信息
        firstChess.transform.parent = go.transform;
        firstChess.transform.localPosition = new Vector3(0, 0, -0.1f);
        firstChess.transform.localScale = Vector3.one;
        firstChess.name = firstChess.name.Substring(0, 2) + temp[2].ToString() + "," + temp[3].ToString();
        //更新棋盘信息
        GameManager.Instance.chessGridMap[temp[2], temp[3]] = GameManager.Instance.chessGridMap[temp[0], temp[1]];
        GameManager.Instance.chessGridMap[temp[0], temp[1]] = 0;
        //清除
        firstChess = null;
        currentChess = null;
        //棋手轮换
        ChessTips.Instance.TurnTip();
    }


    #region 初始化棋盘操作
    private void ChessBoard()
    {
        ChessBoardGrid();  //生成棋盘格
    }
    //生成棋盘格
    private void ChessBoardGrid()
    {
        GameObject obj = Resources.Load<GameObject>(Consts.ChessGridPrefab);
        GameObject go;
        for (int i = 0; i < Consts.ChessY; i++)
        {
            for (int j = 0; j < Consts.ChessX; j++)
            {
                //棋盘格子
                GameObject tmp = Instantiate(obj, ChessGridParent);
                tmp.transform.localPosition = new Vector3(Consts.StartX + j * Consts.SpacingXY,
                    Consts.StartY - i * Consts.SpacingXY, 0);
                tmp.name = "棋盘" + i.ToString() + "," + j.ToString();
                tmp.tag = Consts.ChessGrid;
                chessGridObject[i, j] = tmp;

                if (i <= 4)   //生成红子
                {
                    go = CreateChess(GameManager.Instance.chessGridMap[i, j], 'b');
                    if (go) go.tag = Consts.ChessRed;
                }
                else          //生成黑子
                {
                    go = CreateChess(GameManager.Instance.chessGridMap[i, j], 'r');
                    if (go) go.tag = Consts.ChessBlack;
                }
                if (go != null)
                {
                    GameManager.Instance.chessObject[i, j] = go;
                    go.transform.parent = tmp.transform;
                    go.transform.localPosition = new Vector3(0, 0, -0.1f);
                    go.name = go.name + i.ToString() + "," + j.ToString();
                    go.transform.localScale = new Vector3(1, 1, 1);
                }
            }
        }
    }
    //生成棋子
    private GameObject CreateChess(int num, char flag)
    {
        if (num == 0) return null;
        Sprite[] temp = flag == 'b' ? readChess : blackChess;
        string[] str = flag == 'b' ? chessRName : chessBName;
        GameObject go = Instantiate(Resources.Load<GameObject>(Consts.ChessPrefab));
        go.GetComponent<Image>().sprite = temp[num - 1];
        go.name = str[num - 1];
        switch (num)
        {
            case 1:
                go.AddComponent<ChessArms>();
                break;
            case 2:
                go.AddComponent<ChessGun>();
                break;
            case 3:
                go.AddComponent<ChessVehicle>();
                break;
            case 4:
                go.AddComponent<ChessHorse>();
                break;
            case 5:
                go.AddComponent<ChessShape>();
                break;
            case 6:
                go.AddComponent<ChessSocial>();
                break;
            case 7:
                go.AddComponent<ChessBoss>();
                break;
        }
        return go;
    }
    #endregion

    //返回模式选择界面
    private void ReturnTrick()
    {
        UIManager.Instance.PopPanel(UIPanelType.DifficultyPanel);
    }
    #region 重写父类函数
    public override void OnUIEnter()
    {
        if (m_canvasGroup == null) m_canvasGroup = GetComponent<CanvasGroup>();
        m_canvasGroup.alpha = 0;
        m_canvasGroup.blocksRaycasts = true;
        gameObject.SetActive(true);
        m_canvasGroup.DOFade(1, .7f);
    }
    public override void OnUIPause()
    {
        m_canvasGroup.blocksRaycasts = false;
        m_canvasGroup.DOFade(0, .7f);
    }
    public override void OnUIResume()
    {
        m_canvasGroup.blocksRaycasts = true;
        m_canvasGroup.DOFade(1, .7f);
    }
    public override void OnUIExit()
    {
        m_canvasGroup.blocksRaycasts = false;
        m_canvasGroup.DOFade(0, .5f).OnComplete(() => { gameObject.SetActive(false); });
    }
    #endregion
}