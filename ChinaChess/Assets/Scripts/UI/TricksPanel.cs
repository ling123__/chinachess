/****************************************************
    文件：StartPanel.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 8:57:36
	功能：游戏模式选择界面
*****************************************************/

using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class TricksPanel : BasePanel 
{
    private CanvasGroup m_canvasGroup;

    private Button btn_Pvp;
    private Button btn_Pve;
    private Button btn_NetPvp;
    private void Start()
    {
        btn_Pvp = transform.Find("PVP").GetComponent<Button>();
        btn_Pve = transform.Find("PVE").GetComponent<Button>();
        btn_NetPvp = transform.Find("NetPVP").GetComponent<Button>();

        btn_Pvp.onClick.AddListener(PVP);
        btn_Pve.onClick.AddListener(PVE);
        btn_NetPvp.onClick.AddListener(NetPVP);
    }

    //局域网双人
    private void PVP()
    {

    }
    //人机
    private void PVE()
    {
        UIManager.Instance.PushPanel(UIPanelType.DifficultyPanel);
    }
    //联网双人
    private void NetPVP()
    {
        if(GameManager.Instance.PlayerData == null)
            UIManager.Instance.PushPanel(UIPanelType.LoginPanel);
        else
        {
            //跳转到对战大厅
        }

    }

    #region 重写父类函数
    public override void OnUIEnter()
    {
        if(m_canvasGroup == null)  m_canvasGroup = GetComponent<CanvasGroup>();
        m_canvasGroup.alpha = 0;
        m_canvasGroup.blocksRaycasts = true;
        gameObject.SetActive(true);
        m_canvasGroup.DOFade(1, .7f);
    }
    public override void OnUIPause()
    {
        m_canvasGroup.blocksRaycasts = false;
        m_canvasGroup.DOFade(0, .7f);
    }
    public override void OnUIResume()
    {
        m_canvasGroup.blocksRaycasts = true;
        m_canvasGroup.DOFade(1, .7f);
    }
    public override void OnUIExit()
    {
        m_canvasGroup.blocksRaycasts = false;
        m_canvasGroup.DOFade(0, .5f).OnComplete(() => { gameObject.SetActive(false); });
    }
    #endregion
}