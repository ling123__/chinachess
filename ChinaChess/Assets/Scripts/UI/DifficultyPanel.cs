/****************************************************
    文件：DifficultyPanel.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 8:58:58
	功能：难度选择界面
*****************************************************/

using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class DifficultyPanel : BasePanel 
{
    private CanvasGroup m_canvasGroup;


    private Button btn_Easy;
    private Button btn_Middle;
    private Button btn_Difficult;
    private Button btn_Return;
    private void Start()
    {
        btn_Easy = transform.Find("Easy").GetComponent<Button>();
        btn_Middle = transform.Find("Middle").GetComponent<Button>();
        btn_Difficult = transform.Find("Difficult").GetComponent<Button>();
        btn_Return = transform.Find("Return").GetComponent<Button>();

        btn_Easy.onClick.AddListener(Easy);
        btn_Middle.onClick.AddListener(Middle);
        btn_Difficult.onClick.AddListener(Difficult);
        btn_Return.onClick.AddListener(ReturnTrick);
    }

    //简易
    private void Easy()
    {
        UIManager.Instance.PushPanel(UIPanelType.GamePanel);
    }
    //中等
    private void Middle()
    {
        UIManager.Instance.PushPanel(UIPanelType.GamePanel);
    }
    //困难
    private void Difficult()
    {
        UIManager.Instance.PushPanel(UIPanelType.GamePanel);
    }
    //返回模式选择界面
    private void ReturnTrick()
    {
        UIManager.Instance.PopPanel(UIPanelType.DifficultyPanel);
    }

    #region 重写父类函数
    public override void OnUIEnter()
    {
        if (m_canvasGroup == null) m_canvasGroup = GetComponent<CanvasGroup>();
        m_canvasGroup.alpha = 0;
        m_canvasGroup.blocksRaycasts = true;
        gameObject.SetActive(true);
        m_canvasGroup.DOFade(1, .7f);
    }
    public override void OnUIPause()
    {
        m_canvasGroup.blocksRaycasts = false;
        m_canvasGroup.DOFade(0, .7f);
    }
    public override void OnUIResume()
    {
        m_canvasGroup.blocksRaycasts = true;
        m_canvasGroup.DOFade(1, .7f);
    }
    public override void OnUIExit()
    {
        m_canvasGroup.blocksRaycasts = false;
        m_canvasGroup.DOFade(0, .5f).OnComplete(() => { gameObject.SetActive(false); });
    }
    #endregion
}