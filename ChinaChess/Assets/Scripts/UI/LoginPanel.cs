/****************************************************
    文件：LoginPanel.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/7/12 9:29:28
	功能：登陆面板
*****************************************************/

using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class LoginPanel : BasePanel 
{
    private CanvasGroup m_canvasGroup;
    private InputField userName;
    private InputField userPass;
    private Button loginBtn;

    private void Start()
    {
        m_canvasGroup = GetComponent<CanvasGroup>();

        userName = transform.Find("Name").GetComponentInChildren<InputField>();
        userPass = transform.Find("Pass").GetComponentInChildren<InputField>();
        loginBtn = transform.Find("Login").GetComponent<Button>();

        loginBtn.onClick.AddListener(Login);
    }

    private void Login()
    {
        string acct = userName.text;
        string pass = userPass.text;

        if(acct != "" && pass != "")
        {
            NetMsg msg = new NetMsg
            {
                cmd = (int)CMD.ReqLogin,
                reqLogin = new ReqLogin
                {
                    acct = acct,
                    pass = pass
                }
            };
            NetSvc.Instance.SendMsg(msg);
        }
    }
    #region 重写父类函数
    public override void OnUIEnter()
    {
        if (m_canvasGroup == null) m_canvasGroup = GetComponent<CanvasGroup>();
        m_canvasGroup.alpha = 0;
        m_canvasGroup.blocksRaycasts = true;
        gameObject.SetActive(true);
        m_canvasGroup.DOFade(1, .7f);
    }
    public override void OnUIPause()
    {
        m_canvasGroup.blocksRaycasts = false;
        m_canvasGroup.DOFade(0, .7f);
    }
    public override void OnUIResume()
    {
        m_canvasGroup.blocksRaycasts = true;
        m_canvasGroup.DOFade(1, .7f);
    }
    public override void OnUIExit()
    {
        m_canvasGroup.blocksRaycasts = false;
        m_canvasGroup.DOFade(0, .5f).OnComplete(() => { gameObject.SetActive(false); });
    }
    #endregion
}