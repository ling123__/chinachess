/****************************************************
    文件：GamePanel.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 8:59:11
	功能：游戏界面
*****************************************************/

using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class GamePanel : BasePanel
{
    public Sprite[] blackChess;
    public Sprite[] readChess;
    public Transform ChessGridParent;
    //public Transform blackParent;
    public GameObject EasyAI;

    private CanvasGroup m_canvasGroup;
    private Button btn_Return;
    private GameObject[,] chessGridObject = new GameObject[10, 9];   //棋盘
    
    
    private string[] chessBName = { "黑兵", "黑炮", "黑车", "黑马", "黑象", "黑士", "黑将", };
    private string[] chessRName = { "红兵", "红炮", "红车", "红马", "红象", "红士", "红将", };

    private GameObject currentChess;
    private GameObject firstChess;
    private void Start()
    {
        //ChessTips.Instance.SetText("lalal");

        btn_Return = transform.Find("Return").GetComponent<Button>();

        btn_Return.onClick.AddListener(ReturnTrick);
        GameManager.Instance.chessGridMap = LoadFile.Instance.MapInfo;
        ChessBoard();    //生成棋盘
        //TODO 开始游戏动画
        //ChessTips.Instance.SetText("bilibili");

    }

    private void Update()
    {
        if (!GameManager.Instance.isGameOver && GameManager.Instance.isRobot == false)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    //根据tag分别下不同的棋
                    if (hit.transform.tag == Consts.ChessGrid)
                    {
                        PlayGridChess(hit.transform.gameObject);
                    }
                    else if (hit.transform.tag == Consts.ChessBlack)
                    {
                        PlayBlackChess(hit.transform.gameObject);
                    }
                    else if (hit.transform.tag == Consts.ChessRed)
                    {
                        PlayRedChess(hit.transform.gameObject);
                    }
                }
            }
        }
        else if(!GameManager.Instance.isGameOver && GameManager.Instance.isRobot == true)
        {
            //Debug.Log("机器" + GameManager.Instance.isRobot);
            EasyAI.GetComponent<EasyAI>().SearchAGoodMove(GameManager.Instance.chessObject);
        }
    }
    private void PlayGridChess(GameObject go)
    {
        if (firstChess == null) return;
        int[] temp = ParseStringToInt(firstChess, go);
        GameManager.Instance.chessObject[temp[0], temp[1]] = null;
        
        //如果可以下棋
        if (firstChess.GetComponent<ChessInfo>().PlayChessStyle(temp[0], temp[1], temp[2], temp[3]))
        {
            GameManager.Instance.chessObject[temp[2], temp[3]] = firstChess;
            //如果双将碰面
            if (firstChess.GetComponent<ChessInfo>().JudgeChess(temp[2], temp[3])
                || firstChess.GetComponent<ChessInfo>().EatBoss(temp[2],temp[3]))
            {
                ChessTips.Instance.WinTip();
            }

            //firChess的父物体修改为当前棋盘格子，属性初始化
            MoveInit(go,temp);
            GameManager.Instance.isRobot = true;
        }
    }
    private void PlayBlackChess(GameObject go)
    {
        if (!GameManager.Instance.isGameOver)
        {
            if (GameManager.Instance.isTurn)  //开始
            {
                firstChess = go;
            }
            else
            {
                currentChess = go;
                if (firstChess != null)
                {
                    int[] temp = ParseStringToInt(firstChess, currentChess);
                    if (firstChess.GetComponent<ChessInfo>().PlayChessStyle(temp[0], temp[1], temp[2], temp[3]))
                    {
                        Destroy(currentChess);
                        GameManager.Instance.chessObject[temp[0], temp[1]] = null;
                        GameManager.Instance.chessObject[temp[2], temp[3]] = firstChess;
                        //如果吃到了敌方的主将，或者两将碰面游戏结束
                        if (firstChess.GetComponent<ChessInfo>().JudgeChess(temp[2], temp[3])
                            || firstChess.GetComponent<ChessInfo>().EatBoss(temp[2],temp[3]))
                        {
                            ChessTips.Instance.WinTip();
                        }
                        //TODO 被吃了
                        MoveInit(currentChess.transform.parent.gameObject, temp);
                        GameManager.Instance.isRobot = true;
                    }
                }
            }
        }
    }
    private void PlayRedChess(GameObject go)
    {
        if (!GameManager.Instance.isGameOver)
        {
            if (!GameManager.Instance.isTurn)  //开始默认是红方先下
            {
                firstChess = go;
            }
            else
            {
                currentChess = go;
                if (firstChess != null)
                {
                    int[] temp = ParseStringToInt(firstChess, currentChess);
                    if (firstChess.GetComponent<ChessInfo>().PlayChessStyle(temp[0], temp[1], temp[2], temp[3]))
                    {
                        Destroy(currentChess);
                        GameManager.Instance.chessObject[temp[0], temp[1]] = null;
                        GameManager.Instance.chessObject[temp[2], temp[3]] = firstChess;
                        //如果吃到了敌方的主将，或者两将碰面游戏结束
                        if (firstChess.GetComponent<ChessInfo>().JudgeChess(temp[2], temp[3])
                            || firstChess.GetComponent<ChessInfo>().EatBoss(temp[2],temp[3]))
                        {
                            ChessTips.Instance.WinTip();
                        }
                        MoveInit(currentChess.transform.parent.gameObject, temp);
                        //GameManager.Instance.isRobot = true;
                    }
                }
            }
        }
    }
    //提取物体名字中的位置
    private int[] ParseStringToInt(GameObject t1, GameObject t2)
    {
        int[] temp = new int[4];
        temp[0] = Int32.Parse(t1.name.Substring(2, 1));
        temp[1] = Int32.Parse(t1.name.Substring(4, 1));
        temp[2] = Int32.Parse(t2.name.Substring(2, 1));
        temp[3] = Int32.Parse(t2.name.Substring(4, 1));
        return temp;
    }
    //棋子移动后的初始化
    private void MoveInit(GameObject go,int[] temp)
    {
        //更新棋子信息
        firstChess.transform.parent = go.transform;
        firstChess.transform.localPosition = new Vector3(0, 0, -0.1f);
        firstChess.transform.localScale = Vector3.one;
        firstChess.name = firstChess.name.Substring(0, 2) + temp[2].ToString() + "," + temp[3].ToString();
        //更新棋盘信息
        GameManager.Instance.chessGridMap[temp[2], temp[3]] = GameManager.Instance.chessGridMap[temp[0], temp[1]];
        GameManager.Instance.chessGridMap[temp[0], temp[1]] = 0;
        //清除
        firstChess = null;
        currentChess = null;
        //棋手轮换
        GameManager.Instance.isTurn = !GameManager.Instance.isTurn;
        ChessTips.Instance.TurnTip();
    }


    #region 初始化棋盘操作
    private void ChessBoard()
    {
        ChessBoardGrid();  //生成棋盘格
    }
    //生成棋盘格
    private void ChessBoardGrid()
    {
        GameObject obj = Resources.Load<GameObject>(Consts.ChessGridPrefab);
        GameObject go;
        for (int i = 0; i < Consts.ChessY; i++)
        {
            for (int j = 0; j < Consts.ChessX; j++)
            {
                //ChessTips.Instance.SetText("初始化棋盘");
                //棋盘格子
                GameObject tmp = Instantiate(obj, ChessGridParent);
                tmp.transform.localPosition = new Vector3(Consts.StartX + j * Consts.SpacingXY,
                    Consts.StartY - i * Consts.SpacingXY, 0);
                tmp.name = "棋盘" + i.ToString() + "," + j.ToString();
                tmp.tag = Consts.ChessGrid;
                chessGridObject[i, j] = tmp;

                if (i <= 4)   //生成红子
                {
                    go = CreateChess(GameManager.Instance.chessGridMap[i, j], 'b');
                    if (go) go.tag = Consts.ChessRed;
                }
                else          //生成黑子
                {
                    go = CreateChess(GameManager.Instance.chessGridMap[i, j], 'r');
                    if (go) go.tag = Consts.ChessBlack;
                }
                if (go != null)
                {
                    GameManager.Instance.chessObject[i, j] = go;
                    go.transform.parent = tmp.transform;
                    go.transform.localPosition = new Vector3(0, 0, -0.1f);
                    go.name = go.name + i.ToString() + "," + j.ToString();
                    go.transform.localScale = new Vector3(1, 1, 1);
                }
            }
        }
    }
    //生成棋子
    private GameObject CreateChess(int num, char flag)
    {
        if (num == 0) return null;
        Sprite[] temp = flag == 'b' ? readChess : blackChess;
        string[] str = flag == 'b' ? chessRName : chessBName;
        GameObject go = Instantiate(Resources.Load<GameObject>(Consts.ChessPrefab));
        go.GetComponent<Image>().sprite = temp[num - 1];
        go.name = str[num - 1];
        switch (num)
        {
            case 1:
                go.AddComponent<ChessArms>();
                break;
            case 2:
                go.AddComponent<ChessGun>();
                break;
            case 3:
                go.AddComponent<ChessVehicle>();
                break;
            case 4:
                go.AddComponent<ChessHorse>();
                break;
            case 5:
                go.AddComponent<ChessShape>();
                break;
            case 6:
                go.AddComponent<ChessSocial>();
                break;
            case 7:
                go.AddComponent<ChessBoss>();
                break;
        }
        return go;
    }
    #endregion

    //返回模式选择界面
    private void ReturnTrick()
    {
        UIManager.Instance.PopPanel(UIPanelType.DifficultyPanel);
    }
    #region 重写父类函数
    public override void OnUIEnter()
    {
        if (m_canvasGroup == null) m_canvasGroup = GetComponent<CanvasGroup>();
        m_canvasGroup.alpha = 0;
        m_canvasGroup.blocksRaycasts = true;
        gameObject.SetActive(true);
        m_canvasGroup.DOFade(1, .7f);
    }
    public override void OnUIPause()
    {
        m_canvasGroup.blocksRaycasts = false;
        m_canvasGroup.DOFade(0, .7f);
    }
    public override void OnUIResume()
    {
        m_canvasGroup.blocksRaycasts = true;
        m_canvasGroup.DOFade(1, .7f);
    }
    public override void OnUIExit()
    {
        m_canvasGroup.blocksRaycasts = false;
        m_canvasGroup.DOFade(0, .5f).OnComplete(() => { gameObject.SetActive(false); });
    }
    #endregion
}