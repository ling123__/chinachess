/****************************************************
    文件：GameManager.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 9:5:21
	功能：游戏管理
*****************************************************/

using UnityEngine;

public struct Coordinate
{
    public int x, y;
};
public struct ChessPos
{
    public Coordinate from;
    public Coordinate to;
    public int x;
    public int y;
};

public class GameManager : Singleton<GameManager> 
{
    public bool isTurn = false;   //下棋轮换
    public bool isGameOver = false;
    public string gameWinner;
    public int GameLevel = 1;

    public GameObject blackBoss;  //黑将
    public GameObject redBoss;    //红帅
    public bool isRobot = false;

    public GameObject[,] chessObject = new GameObject[10, 9];   //棋子
    public int[,] chessGridMap;   //棋盘
    public ChessPos[,] moveList = new ChessPos[8, 80];  //AI移动列表
    public ChessPos[] relatePosList = new ChessPos[30];  //棋子响相邻位置

    private PlayerData playerData;
    public PlayerData PlayerData { get => playerData; }

    public string currentChess; //当前轮数棋子
    private string chessColor;  //联网时表明本地棋子颜色
    public string ChessColor { get => chessColor; }

    protected override void Awake()
    {
        base.Awake();

        DontDestroyOnLoad(this.gameObject);  
        UIManager.Instance.PushPanel(UIPanelType.TricksPanel);
        isRobot = false;
        Init();
    }
    private void Init()
    {
        NetSvc net = GetComponent<NetSvc>();
        net.Init();
        LoginSys login = GetComponent<LoginSys>();
        login.Init();
    }

    //保存用户数据
    public void SetPlayerData(PlayerData player)
    {
        playerData = player;
    }

    //设置颜色
    public void SetChessColor(string color)
    {
        chessColor = color;
    }
}