/****************************************************
    文件：UIManager.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 9:5:33
	功能：UI管理
*****************************************************/

using System.Collections.Generic;
using UnityEngine;

public enum UIPanelType
{
    GamePanel,
    DifficultyPanel,
    TricksPanel,
    LoginPanel,
    NetPanel,
}
public class UIManager : Singleton<UIManager> 
{
    public GameObject[] UIPanels;

    private Transform canvasTrans;

    private Dictionary<UIPanelType, BasePanel> PanelDic = new Dictionary<UIPanelType, BasePanel>();
    //页面栈
    private Stack<BasePanel> PanelStack = new Stack<BasePanel>();

    public Transform CanvasTrans
    {
        get
        {
            if (canvasTrans == null) canvasTrans = GameObject.Find("Canvas").transform;
            return canvasTrans;
        }
    }
    protected override void Awake()
    {
        base.Awake();
    }

    //将页面入栈
    public void PushPanel(UIPanelType panelType)
    {
        //判断段栈里面是否有面板，如果有，暂停该面板
        if(PanelStack.Count > 0)
        {
            BasePanel basePanel = PanelStack.Peek();
            basePanel.OnUIPause();
        }

        BasePanel panel = GetPanel(panelType);
        panel.OnUIEnter();
        PanelStack.Push(panel);   //入栈
    }
    //将页面出栈
    public void PopPanel(UIPanelType panelType)
    {
        if (PanelStack.Count <= 0) return;
        BasePanel basePanel = PanelStack.Pop();
        basePanel.OnUIExit();

        if (PanelStack.Count <= 0) return;
        basePanel = PanelStack.Peek();
        basePanel.OnUIResume();   //重新显示下一个页面
    }

    //根据面板类型，返回实例化的面板
    public BasePanel GetPanel(UIPanelType panelType)
    {
        BasePanel panel = PanelDic.TryGet(panelType);

        if(panel == null)  //如果panel为空，面板还没有加载过
        {
            //从配置文件中读取面板路径信息
            string path = LoadFile.Instance.PanelPathDic.TryGet(panelType);
            //生成面板
            GameObject go = Instantiate(Resources.Load<GameObject>(path), CanvasTrans);
            //添加面板
            PanelDic.Add(panelType, go.GetComponent<BasePanel>());
            return go.GetComponent<BasePanel>();
        }
        return panel;
    }

}

public static class DictionaryExtension
{
    public static Tvalue TryGet<Tkey,Tvalue>(this Dictionary<Tkey,Tvalue> dict, Tkey key)
    {
        Tvalue value;
        dict.TryGetValue(key, out value);
        return value;
    }
}