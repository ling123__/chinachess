/****************************************************
    文件：ChessTips.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/5 8:14:30
	功能：下棋提示
*****************************************************/

using UnityEngine;
using UnityEngine.UI;

public class ChessTips : Singleton<ChessTips>
{
    private Text txt_tip;
    private void Start()
    {
        txt_tip = GetComponent<Text>();
    }
    //伦次提示
    public void TurnTip()
    {
        if (!GameManager.Instance.isGameOver)
        {
            if (GameManager.Instance.isTurn)
            {
                txt_tip.text = "黑棋走";
                txt_tip.color = Color.black;
            }
            else
            {
                txt_tip.text = "红棋走";
                txt_tip.color = Color.red;
            }
        }
    }
    //胜负提示
    public void WinTip()
    {
        if (!GameManager.Instance.isTurn)
        {
            txt_tip.text = "黑棋获胜";
            txt_tip.color = Color.black;
        }
        else
        {
            txt_tip.text = "红棋获胜";
            txt_tip.color = Color.red;
        }
        GameManager.Instance.isGameOver = true;
    }
    public void SetText(string text)
    {
        txt_tip.text = text;
    }
}