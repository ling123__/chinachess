/****************************************************
    文件：LoadFile.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 10:5:48
	功能：加载文件信息
*****************************************************/

using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;
using System;

public class LoadFile : Singleton<LoadFile> 
{
    public Dictionary<UIPanelType, string> PanelPathDic = new Dictionary<UIPanelType, string>();
    public int[,] MapInfo = new int[10,9];

    protected override void Awake()
    {
        base.Awake();
        ParseUIPanelTypeJson();   //读取面板信息
        ParseWaveFileInfo();      //读取配置文件
    }

    //解析
    public void ParseUIPanelTypeJson()
    {
        TextAsset asset = Resources.Load<TextAsset>(Consts.UIPanelJson);
        List<UIPanelInfo> uiPanelLst = JsonMapper.ToObject<List<UIPanelInfo>>(asset.text);

        foreach (UIPanelInfo item in uiPanelLst)
        {
            PanelPathDic.Add(item.panelType, item.path);
        }
    }

    public void ParseWaveFileInfo()
    {
        FileStream file = new FileStream("Wave.txt", FileMode.Open);
        StreamReader sr = new StreamReader(file);

        string str = sr.ReadLine();
        int index = 0;
        while(str != null)
        {
            for(int i = 0;i < str.Length; i++)
            {
                MapInfo[index,i] = Int32.Parse(str[i].ToString());
            }
            index++;
            str = sr.ReadLine();
        }
    }

}