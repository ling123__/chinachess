/****************************************************
    文件：UIPanelInfo.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 10:7:3
	功能：Nothing
*****************************************************/

using UnityEngine;

public class UIPanelInfo : MonoBehaviour 
{
    public UIPanelType panelType;
    public string panelTypeString
    {
        get { return panelType.ToString(); }
        set
        {
            panelType = (UIPanelType)System.Enum.Parse(typeof(UIPanelType),value);
        }
    }
    public string path;
}