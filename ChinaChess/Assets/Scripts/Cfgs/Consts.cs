/****************************************************
    文件：Consts.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 10:22:23
	功能：全局常量配置
*****************************************************/

using UnityEngine;

public class Consts : MonoBehaviour 
{
    //文件名称
    public const string UIPanelJson = "Cfgs/UiPanel";

    //预制体
    public const string ChessPrefab = "Prefabs/Chess";
    public const string ChessGridPrefab = "Prefabs/ChessGrid";

    //特效
    public const string Attack = "Prefabs/Attack";
    public const string Move = "Prefabs/Move";
    public const string Select = "Prefabs/Select";
    public const string Stay = "Prefabs/Stay";

    //tags
    public const string ChessGrid = "ChessGrid";
    public const string ChessBlack = "ChessBlack";
    public const string ChessRed = "ChessRed";

    //棋盘位置相关
    public const int StartX = -320;
    public const int StartY = 360;
    public const int SpacingXY = 80;
    public const int ChessX = 9;
    public const int ChessY = 10;
}