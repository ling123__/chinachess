/****************************************************
    文件：Singleton.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/2 21:25:2
	功能：单例脚本
*****************************************************/

using UnityEngine;

public class Singleton<T> : MonoBehaviour where T:MonoBehaviour
{
    private static T m_Instance;
    private readonly object obj = "lock";

    public static T Instance { get => m_Instance; }

    protected virtual void Awake()
    {
        if(m_Instance == null)
        {
            lock (obj)
            {
                if(m_Instance == null)
                {
                    m_Instance = this as T;
                }
            }
        }
    }
}