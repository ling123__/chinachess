/****************************************************
    文件：ChessSocial.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 20:27:25
	功能：士
*****************************************************/

using System;
using UnityEngine;

public class ChessSocial : ChessInfo
{
    private void Start()
    {
        chessId = 6;
        if (gameObject.name.Substring(0, 1) == "红")
            chessColor = "r";
        else
            chessColor = "b";
    }
    public override bool PlayChessStyle(int startx, int starty, int endx, int endy)
    {
        //TODO  九宫格内斜着走
        if (chessColor == "r")
        {
            if (endx > 2 || endy < 3 || endy > 5)
                return false;
            if (Mathf.Abs(endx - startx) == 1 && Mathf.Abs(endy - starty) == 1)
                return true;
        }
        else {
            if (endx < 7 || endy < 3 || endy > 5)
                return false;
            if (Mathf.Abs(endx - startx) == 1 && Mathf.Abs(endy - starty) == 1)
                return true;
        }
        return false;
    }
    public override bool JudgeChess(int endx, int endy)
    {
        //判断两将是否碰面
        int count = -1;
        int bx = Int32.Parse(GameManager.Instance.blackBoss.name.Substring(2, 1));
        int by = Int32.Parse(GameManager.Instance.blackBoss.name.Substring(4, 1));
        int rx = Int32.Parse(GameManager.Instance.redBoss.name.Substring(2, 1));
        int ry = Int32.Parse(GameManager.Instance.redBoss.name.Substring(4, 1));
        if (by == ry)
        {
            count = 0;
            for (int i = rx + 1; i < bx; i++)
            {
                if (GameManager.Instance.chessObject[i, by] != null)
                    count++;
            }
        }
        if (count == 0)
            return true;
        return false;
    }
    public override void AIPlayChessStyle(int startx, int starty, int depth, ref int count)
    {
        base.AIPlayChessStyle(startx, starty, depth, ref count);
    }
    public override bool EatBoss(int endx, int endy)
    {
        return base.EatBoss(endx, endy);
    }
}