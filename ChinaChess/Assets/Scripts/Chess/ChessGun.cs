/****************************************************
    文件：ChessGun.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 20:25:52
	功能：炮
*****************************************************/

using System;
using UnityEngine;

public class ChessGun : ChessInfo
{
    private void Start()
    {
        chessId = 2;
        if (gameObject.name.Substring(0, 1) == "红")
            chessColor = "r";
        else
            chessColor = "b";
    }
    public override bool PlayChessStyle(int startx, int starty, int endx, int endy)
    {
        int count = 0;
        //TODO  间隔一个
        if (startx == endx)
        {
            for (int i = starty > endy ? endy + 1 : starty + 1; i < (starty > endy ? starty : endy); i++)
            {
                if (GameManager.Instance.chessObject[startx, i] != null)
                {
                    count++;
                }
            }
            if (GameManager.Instance.chessObject[endx, endy] == null)
            {
                if (count == 0)
                    return true;
                else
                    return false;
            }
        }
        else if (starty == endy)
        {
            for (int i = startx > endx ? endx + 1 : startx + 1; i < (startx > endx ? startx : endx); i++)
            {
                if (GameManager.Instance.chessObject[i, starty] != null)
                {
                    count++;
                }
            }
            if (GameManager.Instance.chessObject[endx, endy] == null)
            {
                if (count == 0)
                    return true;
                else
                    return false;
            }
        }
        if (count == 1)
            return true;
        return false;
    }
    public override bool JudgeChess(int endx, int endy)
    {
        //判断两将是否碰面
        int count = -1;
        int bx = Int32.Parse(GameManager.Instance.blackBoss.name.Substring(2, 1));
        int by = Int32.Parse(GameManager.Instance.blackBoss.name.Substring(4, 1));
        int rx = Int32.Parse(GameManager.Instance.redBoss.name.Substring(2, 1));
        int ry = Int32.Parse(GameManager.Instance.redBoss.name.Substring(4, 1));
        if (by == ry)
        {
            count = 0;
            for (int i = rx + 1; i < bx; i++)
            {
                if (GameManager.Instance.chessObject[i, by] != null)
                    count++;
            }
        }
        if (count == 0)
            return true;
        return false;
    }

    public override void AIPlayChessStyle(int startx, int starty, int depth, ref int count)
    {
        base.AIPlayChessStyle(startx, starty, depth, ref count);
    }

    public override bool EatBoss(int endx, int endy)
    {
        return base.EatBoss(endx, endy);
    }
}