/****************************************************
    文件：ChessVehicle.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 20:26:18
	功能：车
*****************************************************/

using System;
using UnityEngine;

public class ChessVehicle : ChessInfo 
{
    private void Start()
    {
        chessId = 3;
        if (gameObject.name.Substring(0, 1) == "红")
            chessColor = "r";
        else
            chessColor = "b";
    }
    public override bool PlayChessStyle(int startx, int starty, int endx, int endy)
    {
        bool isMove = false;
        //TODO  间隔一个
        if (startx == endx)
        {
            isMove = true;
            for (int i = starty > endy ? endy + 1 : starty + 1; i < (starty > endy ? starty : endy); i++)
            {
                if (GameManager.Instance.chessObject[startx, i] != null)
                {
                    isMove = false;
                    break;
                }
            }
        }
        else if (starty == endy)
        {
            isMove = true;
            for (int i = startx > endx ? endx + 1 : startx + 1; i < (startx > endx ? startx : endx); i++)
            {
                if (GameManager.Instance.chessObject[i, starty] != null)
                {
                    isMove = false;
                    break;
                }
            }
        }
        if (isMove) return true;
        return false;
    }
    public override bool JudgeChess(int endx, int endy)
    {
        //判断两将是否碰面
        int count = -1;
        int bx = Int32.Parse(GameManager.Instance.blackBoss.name.Substring(2, 1));
        int by = Int32.Parse(GameManager.Instance.blackBoss.name.Substring(4, 1));
        int rx = Int32.Parse(GameManager.Instance.redBoss.name.Substring(2, 1));
        int ry = Int32.Parse(GameManager.Instance.redBoss.name.Substring(4, 1));
        if (by == ry)
        {
            count = 0;
            for (int i = rx + 1; i < bx; i++)
            {
                if (GameManager.Instance.chessObject[i, by] != null)
                    count++;
            }
        }
        if (count == 0)  //两将碰面
            return true;
        return false;
    }

    public override void AIPlayChessStyle(int startx, int starty, int depth, ref int count)
    {
        base.AIPlayChessStyle(startx, starty, depth, ref count);
    }

    public override bool EatBoss(int endx, int endy)
    {
        return base.EatBoss(endx, endy);
    }
}