/****************************************************
    文件：ChessInfo.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 20:4:7
	功能：棋子父类
*****************************************************/

using UnityEngine;

public class ChessInfo : MonoBehaviour
{
    public string chessColor { get; set; }
    public int chessId { get; set; }

    public virtual bool PlayChessStyle(int startx, int starty, int endx, int endy)
    {
        return false;
    }
    public virtual bool JudgeChess(int endx, int endy)
    {
        return false;
    }
    public virtual bool EatBoss(int endx, int endy)
    {
        if (chessColor == "r")
        {
            //判断是否吃了Boss
            if (GameManager.Instance.chessObject[endx, endy]
                == GameManager.Instance.blackBoss)
                return true;
        }
        else
        {
            if (GameManager.Instance.chessObject[endx, endy]
                == GameManager.Instance.redBoss)
                return true;
        }
        return false;
    }
    public virtual void AIPlayChessStyle(int startx, int starty, int depth, ref int count)
    {
        for (int i = 0; i < Consts.ChessY; i++)
        {
            for (int j = 0; j < Consts.ChessX; j++)
            {
                if (i == startx && j == starty) continue;
                //AI的同种花色判断
                if (GameManager.Instance.chessObject[i, j] != null)
                    if (GameManager.Instance.chessObject[i, j].GetComponent<ChessInfo>().chessColor == chessColor)
                        continue;
                if (PlayChessStyle(startx, starty, i, j) && !JudgeChess(i, j)
                    || (PlayChessStyle(startx, starty, i, j) && EatBoss(i, j)))
                {
                    if (depth >= 0)
                        AddMove(startx, starty, i, j, depth, ref count);
                    else
                    {
                        AddPos(i, j, ref count);
                    }
                }
            }
        }
    }

    //添加路径
    public void AddMove(int startx, int starty, int endx, int endy, int depth, ref int count)
    {
        GameManager.Instance.moveList[depth, count].from.x = startx;
        GameManager.Instance.moveList[depth, count].from.y = starty;
        GameManager.Instance.moveList[depth, count].to.x = endx;
        GameManager.Instance.moveList[depth, count].to.y = endy;
        count++;
    }

    //添加相邻
    public void AddPos(int endx, int endy, ref int count)
    {
        GameManager.Instance.relatePosList[count].x = endx;
        GameManager.Instance.relatePosList[count].y = endy;
        count++;
        //Debug.Log(chessId+" "+ chessColor+" " + "周边棋子数量：" + count + " "+endx+","+endy);
    }
}