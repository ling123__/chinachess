/****************************************************
    文件：ChessArms.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 20:25:28
	功能：兵，卒
*****************************************************/

using System;
using UnityEngine;

public class ChessArms : ChessInfo
{
    private GameObject go;
    private void Start()
    {
        chessId = 1;
        if (gameObject.name.Substring(0, 1) == "红")
            chessColor = "r";
        else
            chessColor = "b";
    }
    public override bool PlayChessStyle(int startx, int starty, int endx, int endy)
    {
        //一直向前走，过了和可以向左向右走
        if(chessColor == "r")
        {
            //TODO显示所有当前棋子可以走的路径
            if(startx >= 5)
            {
                if ((endx - startx == 1 && starty == endy) || (startx == endx && Mathf.Abs(starty - endy) == 1))
                    return true;
            }
            else
            {
                if (endx - startx == 1 && starty == endy)
                    return true;
            }
        }
        else if(chessColor == "b")
        {
            if(startx <= 4)  //过河
            {
                if ((startx - endx == 1 && starty == endy) || (startx == endx && Mathf.Abs(starty - endy) == 1))
                    return true;
            }
            else  //没过河
            {
                if (startx - endx == 1 && starty == endy)
                    return true;
            }
        }
        return false;
    }
    public override bool JudgeChess(int endx, int endy)
    {
        //判断两将是否碰面
        int count = -1;
        int bx = Int32.Parse(GameManager.Instance.blackBoss.name.Substring(2, 1));
        int by = Int32.Parse(GameManager.Instance.blackBoss.name.Substring(4, 1));
        int rx = Int32.Parse(GameManager.Instance.redBoss.name.Substring(2, 1));
        int ry = Int32.Parse(GameManager.Instance.redBoss.name.Substring(4, 1));
        if(by == ry)
        {
            count = 0;
            for(int i = rx + 1;i < bx; i++)
            {
                if (GameManager.Instance.chessObject[i, by] != null)
                    count++;
            }
        }
        if (count == 0)
            return true;
        return false;
    }

    //public override void AIPlayChessStyle(int startx, int starty,int depth,ref int count)
    //{
    //    if(chessColor == "r")
    //    {
    //        for(int i = 0;i < 3; i++)
    //        {
    //            for(int j = 3;j < 6; j++)
    //            {
    //                if(PlayChessStyle(startx,starty,i,j) && !JudgeChess(i, j))
    //                {
    //                    if (depth >= 0)
    //                        AddMove(startx, starty, i, j, depth, ref count);
    //                    else
    //                        AddPos(i, j, ref count);
    //                }
    //            }
    //        }
    //    }
    //    else if(chessColor == "b")
    //    {
    //        for (int i = 7; i < 10; i++)
    //        {
    //            for (int j = 3; j < 6; j++)
    //            {
    //                if (PlayChessStyle(startx, starty, i, j) && !JudgeChess(i, j))
    //                {
    //                    if (depth >= 0)
    //                        AddMove(startx, starty, i, j, depth, ref count);
    //                    else
    //                        AddPos(i, j, ref count);
    //                }
    //            }
    //        }
    //    }
    //}  

    public override void AIPlayChessStyle(int startx, int starty, int depth, ref int count)
    {
        base.AIPlayChessStyle(startx, starty, depth, ref count);
    }

    public override bool EatBoss(int endx, int endy)
    {
        return base.EatBoss(endx, endy);
    }
}