/****************************************************
    文件：ChessHorse.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 20:26:35
	功能：马
*****************************************************/

using System;
using UnityEngine;

public class ChessHorse : ChessInfo
{
    private int[] dx = { -1, 1, -2, 2 };
    private int[] dy = { 2, -2, 1, -1 };
    private void Start()
    {
        chessId = 4;
        if (gameObject.name.Substring(0, 1) == "红")
            chessColor = "r";
        else
            chessColor = "b";
    }
    public override bool PlayChessStyle(int startx, int starty, int endx, int endy)
    {
        //TODO  马走日
        for (int i = 0; i < 4; i++)
        {
            if (Mathf.Abs(dx[i]) == 1)
            {
                for (int j = 0; j < 2; j++)
                {
                    int xx = startx + dx[i];
                    int yy = starty + dy[j];
                    if (xx == endx && yy == endy)
                    {
                        int index = dy[j] == 2 ? 1 : -1;

                        if (GameManager.Instance.chessObject[startx, starty + index] != null)
                            return false;
                        return true;
                    }
                }
            }
            else
            {
                for (int j = 2; j < 4; j++)
                {
                    int xx = startx + dx[i];
                    int yy = starty + dy[j];
                    if (xx == endx && yy == endy)
                    {
                        int index = dx[i] == -2 ? -1 : 1;
                        if (GameManager.Instance.chessObject[startx + index, starty] != null)
                            return false;
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public override bool JudgeChess(int endx, int endy)
    {
        //判断两将是否碰面
        int count = -1;
        int bx = Int32.Parse(GameManager.Instance.blackBoss.name.Substring(2, 1));
        int by = Int32.Parse(GameManager.Instance.blackBoss.name.Substring(4, 1));
        int rx = Int32.Parse(GameManager.Instance.redBoss.name.Substring(2, 1));
        int ry = Int32.Parse(GameManager.Instance.redBoss.name.Substring(4, 1));
        if (by == ry)
        {
            count = 0;
            for (int i = rx + 1; i < bx; i++)
            {
                if (GameManager.Instance.chessObject[i, by] != null)
                    count++;
            }
        }
        if (count == 0)
            return true;
        return false;
    }

    public override void AIPlayChessStyle(int startx, int starty, int depth, ref int count)
    {
        base.AIPlayChessStyle(startx, starty, depth, ref count);
    }

    public override bool EatBoss(int endx, int endy)
    {
        return base.EatBoss(endx, endy);
    }
}