/****************************************************
    文件：ChessBoss.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/3 20:27:39
	功能：将、帅
*****************************************************/

using System;
using UnityEngine;

public class ChessBoss : ChessInfo 
{
    private void Start()
    {
        chessId = 7;
        if (gameObject.name.Substring(0, 1) == "红")
        {
            chessColor = "r";
            GameManager.Instance.redBoss = gameObject;
        }
        else
        {
            chessColor = "b";
            GameManager.Instance.blackBoss = gameObject;
        }
    }
    public override bool PlayChessStyle(int startx, int starty, int endx, int endy)
    {
        //TODO  只能在九宫格以内移动
        if (chessColor == "r")
        {
            if (endx > 2 || endy < 3 || endy > 5)
                return false;
            if ((Mathf.Abs(endx - startx) == 1 && endy == starty)
                || (Mathf.Abs(endy - starty) == 1 && endx == startx))
                return true;
        }
        else
        {
            if (endx < 7 || endy < 3 || endy > 5)
                return false;
            if ((Mathf.Abs(endx - startx) == 1 && endy == starty)
                || (Mathf.Abs(endy - starty) == 1 && endx == startx))
                return true;
        }
        return false;
    }
    public override bool JudgeChess(int endx, int endy)
    {
        int count = -1;
        if (chessColor == "r")
        {
            int bx = Int32.Parse(GameManager.Instance.blackBoss.name.Substring(2, 1));
            int by = Int32.Parse(GameManager.Instance.blackBoss.name.Substring(4, 1));
            if(endy == by)
            {
                count = 0;
                for (int i = endx + 1;i < bx; i++)
                {
                    if (GameManager.Instance.chessObject[i, endy] != null)
                        count++;
                }
            }
        }
        else
        {
            int rx = Int32.Parse(GameManager.Instance.redBoss.name.Substring(2, 1));
            int ry = Int32.Parse(GameManager.Instance.redBoss.name.Substring(4, 1));
            if (endy == ry)
            {
                count = 0;
                for (int i = rx + 1; i < endx; i++)
                {
                    if (GameManager.Instance.chessObject[i, endy] != null)
                        count++;
                }
            }
        }
        if (count == 0)
            return true;
        return false;
    }

    public override void AIPlayChessStyle(int startx, int starty, int depth, ref int count)
    {
        base.AIPlayChessStyle(startx, starty, depth, ref count);
    }
}