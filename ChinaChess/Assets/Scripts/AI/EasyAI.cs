/****************************************************
    文件：EasyAI.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/6/6 9:26:12
	功能：简易AI 每个都有一个基础值，威胁值，保护值，灵活值
*****************************************************/

using System.Collections.Generic;
using UnityEngine;

public class EasyAI : Singleton<EasyAI>
{
    public GameObject[,] chessObject = new GameObject[10, 9];

    private int searchLevel;
    private int moveCount;
    private int posCount;
    private ChessPos bestStep;   //当前搜出的最佳移动算法

    //棋子的基础价值
    private int[] baseValue = new int[15]
    {
        0,100,450,900,400,200,200,10000,  //兵，炮，车，马，象，士，将
          100,450,900,400,200,200,10000,
    };

    //棋子的灵活值
    private int[] flexValue = new int[15]
    {
        0,1,6,6,12,1,1,0,    //兵，炮，车，马，象，士，将
          1,6,6,12,1,1,0,
    };

    //红兵攻击位置附加值
    private int[,] r_bingValue = new int[10, 9]
    {
        {0,0,0,0,0,0,0,0,0} ,
        {90,90,110,120,120,120,110,90,90} ,
        {90,90,110,120,120,120,110,90,90} ,
        {70,90,110,110,110,110,110,90,70} ,
        {70,70,70,70,70,70,70,70,70} ,
        {0,0,0,0,0,0,0,0,0} ,
        {0,0,0,0,0,0,0,0,0} ,
        {0,0,0,0,0,0,0,0,0} ,
        {0,0,0,0,0,0,0,0,0} ,
        {0,0,0,0,0,0,0,0,0} ,
    };

    //黑卒攻击位置附加值
    private int[,] b_bingValue = new int[10, 9]
    {
        {0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0},
        {70,70,70,70,70,70,70,70,70},
        {70,90,110,110,110,110,110,90,70},
        {90,90,110,120,120,120,110,90,90},
        {90,90,110,120,120,120,110,90,90},
        {0,0,0,0,0,0,0,0,0},
    };

    //过程中棋盘的信息
    private int[,] attackPos;  //每个位置的威胁值
    private int[,] guardPos;   //每个位置被保护信息
    private int[,] flexPos;    //每个位置的灵活值
    private int[,] chessValue; //存放每个位置的棋子总价值

    private void Start()
    {
        searchLevel = GameManager.Instance.GameLevel;
    }

    public void SearchAGoodMove(GameObject[,] chess)
    {
        chessObject = chess;
        //NegaMax(searchLevel);
        //AlphaBeta(searchLevel,-20000,20000);
        //MergeSortAlphaBeta(searchLevel, -20000, 20000);
        //DesireSearch();
        PrincipalVariation(searchLevel, -20000, 20000);
        //来的位置，去的位置
        GameObject go1 = GameManager.Instance.chessObject[bestStep.from.x, bestStep.from.y];
        GameObject go2 = GameManager.Instance.chessObject[bestStep.to.x, bestStep.to.y];

        if (go2 == null)  //移动位置是空白
        {
            Transform temp = GameObject.Find("棋盘" + bestStep.to.x.ToString() + "," + bestStep.to.y.ToString()).GetComponent<Transform>();
            MoveInit(go1, temp);
        }
        else
        {
            //if(go1.GetComponent<ChessInfo>().PlayChessStyle(bestStep.from.x, bestStep.from.y, bestStep.to.x, bestStep.to.y)
            //    ||go1.GetComponent<ChessInfo>().EatBoss(bestStep.to.x, bestStep.to.y))
            //{
            //    ChessTips.Instance.WinTip();
            //}
            MoveInit(go1, go2.transform.parent);
            Destroy(go2);
        }
    }
    //移动位置后初始化
    private void MoveInit(GameObject go, Transform temp)
    {
        go.transform.parent = temp;
        go.transform.localPosition = new Vector3(0, 0, -0.1f);
        go.transform.localScale = Vector3.one;
        go.name = go.name.Substring(0, 2) + bestStep.to.x.ToString() + "," + bestStep.to.y.ToString();

        GameManager.Instance.chessObject[bestStep.from.x, bestStep.from.y] = null;
        GameManager.Instance.chessObject[bestStep.to.x, bestStep.to.y] = go;
        //更新棋盘信息
        GameManager.Instance.chessGridMap[bestStep.to.x, bestStep.to.y]
            = GameManager.Instance.chessGridMap[bestStep.from.x, bestStep.from.y];
        GameManager.Instance.chessGridMap[bestStep.from.x, bestStep.from.y] = 0;
        //轮换
        GameManager.Instance.isTurn = !GameManager.Instance.isTurn;
        ChessTips.Instance.TurnTip();
        GameManager.Instance.isRobot = false;
    }
    #region  搜索算法
    #region 负极值搜索
    public int NegaMax(int depth)
    {
        int best = -20000;   //初始化为负无穷
        int score;           //当前棋盘的分数
        int count = 0;
        GameObject go = null;

        if (depth <= 0)
        {
            return Evaluate((searchLevel - depth) % 2 != 0);
        }
        //列举当前局面下一步所有可能走的着法
        count = CreatePossibleMove(depth, (searchLevel - depth) % 2 != 0);
        //枚举所有可能的走法
        for (int i = 0; i < count; i++)
        {
            //根据当前的走法，产生新的棋盘
            go = MakeNextMove(GameManager.Instance.moveList[depth, i]);
            //递归调用负极大值搜索下一层
            score = -NegaMax(depth - 1);
            UnMakeMove(GameManager.Instance.moveList[depth, i], go);
            if (score > best)
            {
                best = score;
                if (depth == searchLevel)
                {
                    //TODO  记录当前棋盘最佳走法
                    bestStep = GameManager.Instance.moveList[depth, i];
                }
            }
        }
        return best;
    }
    #endregion

    #region alpha-beta剪枝算法
    private int AlphaBeta(int depth, int alpha, int beta)
    {
        int score;           //当前棋盘的分数
        int count = 0;
        GameObject go = null;

        if (depth <= 0)
        {
            return Evaluate((searchLevel - depth) % 2 != 0);
        }
        //列举当前局面下一步所有可能走的着法
        count = CreatePossibleMove(depth, (searchLevel - depth) % 2 != 0);
        //枚举所有可能的走法
        for (int i = 0; i < count; i++)
        {
            //根据当前的走法，产生新的棋盘
            go = MakeNextMove(GameManager.Instance.moveList[depth, i]);
            //递归调用负极大值搜索下一层
            score = -AlphaBeta(depth - 1, -beta, -alpha);
            UnMakeMove(GameManager.Instance.moveList[depth, i], go);
            //剪枝
            if (score >= beta) return beta;

            if (score > alpha)
            {
                alpha = score;    //保存最大值
                if (depth == searchLevel)
                {
                    //TODO  记录当前棋盘最佳走法
                    bestStep = GameManager.Instance.moveList[depth, i];
                }
            }
        }
        return alpha;
    }
    #endregion

    #region alpha-beta剪枝优化算法
    //历史启发  键：着法  值：历史得分
    private Dictionary<ChessPos, int> histroyDic = new Dictionary<ChessPos, int>();
    private int MergeSortAlphaBeta(int depth, int alpha, int beta)
    {
        int score;           //当前棋盘的分数
        int count = 0;
        GameObject go = null;

        if (depth <= 0)
        {
            return Evaluate((searchLevel - depth) % 2 != 0);
        }
        //列举当前局面下一步所有可能走的着法
        count = CreatePossibleMove(depth, (searchLevel - depth) % 2 != 0);
        MergeSort(GameManager.Instance.moveList, count, depth);
        //枚举所有可能的走法
        for (int i = 0; i < count; i++)
        {
            //根据当前的走法，产生新的棋盘
            go = MakeNextMove(GameManager.Instance.moveList[depth, i]);
            //递归调用负极大值搜索下一层
            score = -MergeSortAlphaBeta(depth - 1, -beta, -alpha);
            UnMakeMove(GameManager.Instance.moveList[depth, i], go);
            //剪枝
            if (score >= beta) return beta;

            if (score > alpha)
            {
                alpha = score;    //保存最大值
                if (depth == searchLevel)
                {
                    //TODO  记录当前棋盘最佳走法
                    bestStep = GameManager.Instance.moveList[depth, i];
                    AddHistroyScore(bestStep, depth);
                }
            }
        }
        return alpha;
    }
    //添加历史启发
    private void AddHistroyScore(ChessPos move, int depth)
    {
        //如果历史启发中有，则增加值
        if (histroyDic.TryGetValue(move, out int score))
        {
            histroyDic[move] += 2 << depth;
        }
        else   //没有则添加
        {
            histroyDic.Add(move, 2 << depth);
        }
    }
    //取得指定着法的分值
    private int GetHistroyScore(ChessPos move)
    {
        histroyDic.TryGetValue(move, out int score);
        return score;
    }
    //归并排序
    private void MergeSort(ChessPos[,] move, int count, int depth)
    {
        ChessPos[,] temp = new ChessPos[8, 80];
        Sort(move, 0, count, temp, depth);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="move"></param>
    /// <param name="startx">分段数组开始的位置</param>
    /// <param name="endx">分段数组结束的位置</param>
    /// <param name="temp">临时数组</param>
    /// <param name="depth">深度</param>
    private void Sort(ChessPos[,] move, int startx, int endx, ChessPos[,] temp, int depth)
    {
        if (startx < endx)
        {
            int mid = (startx + endx) / 2;
            //对左边归并排序，
            Sort(move, startx, mid, temp, depth);
            //对右边归并排序
            Sort(move, mid + 1, endx, temp, depth);
            //将两个有序数组进行合并
            Merge(move, startx, mid, endx, temp, depth);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="move">移动列表数组</param>
    /// <param name="startx">分段数组开始位置</param>
    /// <param name="mid">分段数组重点</param>
    /// <param name="endx">分段数组结束位置</param>
    /// <param name="temp">临时数组</param>
    /// <param name="depth">深度</param>
    private void Merge(ChessPos[,] move, int startx, int mid, int endx, ChessPos[,] temp, int depth)
    {
        int i = startx, j = mid + 1;
        int k = 0;
        while (i <= mid && j <= endx)
        {
            //取出两个数组中较大的历史得分放到临时数组
            int num1 = GetHistroyScore(move[depth, i]);
            int num2 = GetHistroyScore(move[depth, j]);
            temp[depth, k++] = num1 > num2 ? move[depth, i++] : move[depth, j++];
        }
        //将左边的剩余元素放入临时数组
        while (i <= mid)
        {
            temp[depth, k++] = move[depth, i++];
        }
        //将右边的剩余元素放入临时数组
        while (j <= endx)
        {
            temp[depth, k++] = move[depth, j++];
        }
        k = 0;
        //将临时数组中的元素填充
        while (startx < endx)
        {
            move[depth, startx++] = temp[depth, k++];
        }
    }
    #endregion

    #region 渴望算法
    private void DesireSearch()
    {
        int X = 0;  //深度N-1层的最佳得分
        int current;   //搜索结果
        searchLevel = GameManager.Instance.GameLevel - 1;
        X = FalphaBeta(searchLevel, -20000, 20000);
        //对目标小窗进行搜索
        searchLevel = GameManager.Instance.GameLevel;
        current = FalphaBeta(searchLevel, X - 50, X + 50);
        //估计值偏小
        if (current < X - 50)
        {
            FalphaBeta(searchLevel, -20000, X - 50);
        }
        //估计值偏大
        if (current > X + 50)
        {
            FalphaBeta(searchLevel, X + 50, 20000);
        }
    }
    //渴望搜索
    private int FalphaBeta(int depth, int alpha, int beta)
    {
        int score;           //当前棋盘的分数
        int count = 0;
        int current = -20000;
        GameObject go = null;

        if (depth <= 0)
        {
            return Evaluate((searchLevel - depth) % 2 != 0);
        }
        //列举当前局面下一步所有可能走的着法
        count = CreatePossibleMove(depth, (searchLevel - depth) % 2 != 0);
        //枚举所有可能的走法
        for (int i = 0; i < count; i++)
        {
            //根据当前的走法，产生新的棋盘
            go = MakeNextMove(GameManager.Instance.moveList[depth, i]);
            //递归调用负极大值搜索下一层
            score = -FalphaBeta(depth - 1, -beta, -alpha);
            UnMakeMove(GameManager.Instance.moveList[depth, i], go);

            if (score > current)
            {
                current = score;
                if (score >= alpha)
                {
                    alpha = score;  //更新边界值
                    if (depth == searchLevel)
                    {
                        //TODO  记录当前棋盘最佳走法
                        bestStep = GameManager.Instance.moveList[depth, i];
                    }
                }
                if (alpha >= beta) break;
            }
        }
        return current;
    }
    #endregion

    #region  极大窗口算法
    private int PrincipalVariation(int depth, int alpha, int beta)
    {
        int score;           //当前棋盘的分数
        int count = 0;
        int best = 0;
        GameObject go = null;

        if (depth <= 0)
        {
            return Evaluate((searchLevel - depth) % 2 != 0);
        }
        //列举当前局面下一步所有可能走的着法
        count = CreatePossibleMove(depth, (searchLevel - depth) % 2 != 0);
        //对于第一个节点，按照原来的范围搜索，会得到一个best值
        go = MakeNextMove(GameManager.Instance.moveList[depth, 0]);
        best = -PrincipalVariation(depth - 1, -beta, -alpha);
        UnMakeMove(GameManager.Instance.moveList[depth, 0], go);

        if (depth == searchLevel)
        {
            bestStep = GameManager.Instance.moveList[depth, 0];
        }
        //枚举所有可能的走法
        for (int i = 1; i < count; i++)
        {
            //首先保证不能被剪枝，如果被剪掉了，那么不在考虑这个位置
            if (best < beta)
            {
                //如果当前得分大于alpha，则更新边界值，
                if (best > alpha) alpha = best;
            }
            go = MakeNextMove(GameManager.Instance.moveList[depth, i]);
            //递归调用负极大值搜索下一层
            score = -FalphaBeta(depth - 1, -alpha - 1, -alpha);

            if (score > alpha && score < beta)
            {
                best = -PrincipalVariation(depth - 1, -beta, -score);
                if (depth == searchLevel)
                {
                    //TODO  记录当前棋盘最佳走法
                    bestStep = GameManager.Instance.moveList[depth, i];
                }
            }
            else if(score > best)
            {
                best = score;
                if(depth == searchLevel)
                {
                    bestStep = GameManager.Instance.moveList[depth, i];
                }
            }
            UnMakeMove(GameManager.Instance.moveList[depth, i], go);
        }
        return best;
    }
    #endregion
    #endregion

    //当前棋盘所有棋子可能的移动方法
    private int CreatePossibleMove(int depth, bool side)
    {
        moveCount = 0;
        for (int j = 0; j < Consts.ChessX; j++)   //竖向搜索
        {
            for (int i = 0; i < Consts.ChessY; i++)
            {
                if (chessObject[i, j] != null)
                {
                    if (!side && chessObject[i, j].GetComponent<ChessInfo>().chessColor == "r")
                    {
                        //偶数层AI产生黑棋走法，跳过红棋
                        continue;
                    }
                    if (side && chessObject[i, j].GetComponent<ChessInfo>().chessColor == "b")
                    {
                        //奇数层AI产生红棋走法，跳过黑棋
                        continue;
                    }
                    chessObject[i, j].GetComponent<ChessInfo>().AIPlayChessStyle(i, j, depth, ref moveCount);
                }
            }
        }
        return moveCount;
    }
    //根据当前走法产生新的棋盘
    private GameObject MakeNextMove(ChessPos chessPos)
    {
        GameObject go;
        go = chessObject[chessPos.to.x, chessPos.to.y];
        chessObject[chessPos.to.x, chessPos.to.y] = chessObject[chessPos.from.x, chessPos.from.y];
        chessObject[chessPos.from.x, chessPos.from.y] = null;
        return go;
    }

    //回复当前局面，递归回复
    private void UnMakeMove(ChessPos chessPos, GameObject go)
    {
        chessObject[chessPos.from.x, chessPos.from.y] = chessObject[chessPos.to.x, chessPos.to.y];
        chessObject[chessPos.to.x, chessPos.to.y] = go;
    }

    //评估函数
    private int Evaluate(bool side)
    {
        attackPos = new int[10, 9];
        guardPos = new int[10, 9];
        flexPos = new int[10, 9];
        chessValue = new int[10, 9];
        GameObject currentPos = null;
        GameObject targetPos = null;
        posCount = 0;
        #region 第一次扫描棋盘
        for (int i = Consts.ChessY - 1; i >= 0; i--)
        {
            for (int j = 0; j < Consts.ChessX; j++)
            {
                if (chessObject[i, j] != null)
                {
                    currentPos = chessObject[i, j];  //保存当前位置棋子
                    //找出该位置所有相关位置
                    posCount = 0;
                    chessObject[i, j].GetComponent<ChessInfo>().AIPlayChessStyle(i, j, -1, ref posCount);
                    //对每一个后续位置进行处理
                    for (int k = 0; k < posCount; k++)
                    {
                        int x = GameManager.Instance.relatePosList[k].x;
                        int y = GameManager.Instance.relatePosList[k].y;
                        targetPos = chessObject[x, y];
                        //相关位置是空格
                        if (targetPos == null)
                        {
                            //空格子，代表当前棋子可以走到这个位置，灵活值+1
                            flexPos[x, y]++;
                        }
                        else   //否则是棋子
                        {
                            //如果是乙方的棋子，则保护值+1
                            if (currentPos.GetComponent<ChessInfo>().chessColor == targetPos.GetComponent<ChessInfo>().chessColor)
                            {
                                guardPos[x, y]++;
                            }
                            else  //如果是敌方棋子，威胁值+1，灵活性+1
                            {
                                attackPos[x, y]++;
                                flexPos[x, y]++;

                                if (!side)  //黑方轮次  并且是红帅
                                {
                                    if (targetPos == GameManager.Instance.redBoss)
                                        return 18888;
                                    else
                                        attackPos[x, y] +=
                                            ((baseValue[currentPos.GetComponent<ChessInfo>().chessId]
                                            - targetPos.GetComponent<ChessInfo>().chessId) / 10 + 30) / 10;
                                }
                                else        //红方轮次  并且是黑将
                                {
                                    if (targetPos == GameManager.Instance.blackBoss)
                                        return 18888;
                                    else
                                        attackPos[x, y] +=
                                            ((baseValue[currentPos.GetComponent<ChessInfo>().chessId]
                                            - targetPos.GetComponent<ChessInfo>().chessId) / 10 + 30) / 10;
                                }

                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region 第二次扫描棋盘，对棋盘上每个棋子的自身基础值做处理
        for (int i = Consts.ChessY - 1; i >= 0; i--)
        {
            for (int j = 0; j < Consts.ChessX; j++)
            {
                if (chessObject[i, j] != null)
                {
                    currentPos = chessObject[i, j];
                    //如果是棋子，则该位置灵活值+1
                    chessValue[i, j]++;
                    //每个棋子的灵活值加基础值
                    chessValue[i, j] += flexValue[currentPos.GetComponent<ChessInfo>().chessId] * flexPos[i, j];
                    //如果是兵，则基础值加上兵的位置附加值
                    if (currentPos.GetComponent<ChessInfo>().chessId == 1
                        && currentPos.GetComponent<ChessInfo>().chessColor == "r")
                    {
                        chessValue[i, j] += b_bingValue[i, j];
                    }
                    else if (currentPos.GetComponent<ChessInfo>().chessId == 1
                        && currentPos.GetComponent<ChessInfo>().chessColor == "b")
                    {
                        chessValue[i, j] += r_bingValue[i, j];
                    }
                }
            }
        }
        #endregion

        #region 第三次扫描棋盘  计算当前棋子所在位置的基础总分，（根据增量以及威胁值和保护值计算）
        int delta = 0;
        for (int i = Consts.ChessY - 1; i >= 0; i--)
        {
            for (int j = 0; j < Consts.ChessX; j++)
            {
                if (chessObject[i, j] != null)
                {
                    currentPos = chessObject[i, j];
                    //棋子的1/16价值作为保护/威胁增量
                    delta = baseValue[currentPos.GetComponent<ChessInfo>().chessId] / 16;
                    //基础值加上每个棋子的价值
                    chessValue[i, j] += baseValue[currentPos.GetComponent<ChessInfo>().chessId];
                    //红棋
                    if (currentPos.GetComponent<ChessInfo>().chessColor == "r")
                    {
                        if (attackPos[i, j] != 0)
                        {
                            if (side)   //当前是红方走
                            {
                                //是红帅
                                if (currentPos == GameManager.Instance.redBoss)
                                {
                                    chessValue[i, j] -= 20;
                                }
                                else   //是普通棋子
                                {
                                    chessValue[i, j] -= delta * 2;  //基础值减delta*2
                                    if (guardPos[i, j] != 0)
                                    {
                                        //当前位置被保护，则加增量
                                        chessValue[i, j] += delta;
                                    }
                                }
                            }
                            else  //当前是黑棋走
                            {
                                if (currentPos == GameManager.Instance.redBoss)
                                {
                                    return 18888; //返回一个极大值
                                }
                                else   //是普通棋子
                                {
                                    chessValue[i, j] -= delta * 10;  //基础值减delta*2
                                    if (guardPos[i, j] != 0)
                                    {
                                        //当前位置被保护，则加增量
                                        chessValue[i, j] += delta * 9;
                                    }
                                }
                            }
                        }
                        else   //当前棋子不被威胁
                        {
                            if (guardPos[i, j] != 0)
                                chessValue[i, j] += 5;  //保护值增加
                        }
                    }
                    else   //当前是黑棋走
                    {
                        if (attackPos[i, j] != 0)
                        {
                            if (!side)   //当前是黑方走
                            {
                                if (currentPos == GameManager.Instance.blackBoss)
                                {
                                    chessValue[i, j] -= 20;
                                }
                                else   //是普通棋子
                                {
                                    chessValue[i, j] -= delta * 2;  //基础值减delta*2
                                    if (guardPos[i, j] != 0)
                                    {
                                        //当前位置被保护，则加增量
                                        chessValue[i, j] += delta;
                                    }
                                }
                            }
                            else  //当前是红棋
                            {
                                if (currentPos == GameManager.Instance.blackBoss)
                                {
                                    return 18888; //返回一个极大值
                                }
                                else   //是普通棋子
                                {
                                    chessValue[i, j] -= delta * 10;  //基础值减delta*2
                                    if (guardPos[i, j] != 0)
                                    {
                                        //当前位置被保护，则加增量
                                        chessValue[i, j] += delta * 9;
                                    }
                                }
                            }
                        }
                        else   //当前棋子不被威胁
                        {
                            if (guardPos[i, j] != 0)
                                chessValue[i, j] += 5;  //保护值增加
                        }
                    }
                }
            }
        }
        #endregion

        #region  第四次扫描棋盘，计算红放与黑方的总得分，返回评估值
        int redValue = 0;
        int blackValue = 0;
        for (int i = Consts.ChessY - 1; i >= 0; i--)
        {
            for (int j = 0; j < Consts.ChessX; j++)
            {
                if (chessObject[i, j] != null)
                {
                    currentPos = chessObject[i, j];
                    if (currentPos.GetComponent<ChessInfo>().chessColor == "r")
                        redValue += chessValue[i, j];
                    else
                        blackValue += chessValue[i, j];
                }
            }
        }
        if (side)
            return redValue - blackValue;
        else
            return blackValue - redValue;
        #endregion
    }
}